package com.vpv;

/**
 * Получение текстовых представлений чисел
 *
 * @author Pavel Vasin
 * @since 1.0
 */
public class Phrase {

  /**
   * Получить текстовое представление числа
   *
   * @param digit      Целевое число
   * @param isThousand Это разряд "тысячи" или нет
   * @param index      Индекс: порядковый номер разряда
   * @return Текстовое представление числа
   */
  public static String getPrepared(String digit, boolean isThousand, Integer index) {
    String digitPhrase = Alias.getChunkPhrase(digit, isThousand);
    String digitName = Phrase.getName(digit)[index];
    String result = digitPhrase.equals("") ? "" : digitPhrase.concat(digitName);
    return result;
  }

  /**
   * Получить текстовое представление числа
   *
   * @param digit      Целевое число
   * @param isThousand Это разряд "тысячи" или нет
   * @return Текстовое представление числа
   */
  public static String getPrepared(String digit, boolean isThousand) {
    String result = Alias.getChunkPhrase(digit, isThousand);
    return result.trim();
  }

  /**
   * Получить склонение чисел
   *
   * @param number Число-разряд
   * @return String[] Массив склонений числовых разрядов
   * @since 1.0
   */
  public static String[] getName(String number) {
    String[] result = {};
    String checked = number.equals("") ? "0" : number;
    Integer real = Integer.parseInt(checked);
    String last = checked.substring(checked.length() - 1);
    Integer lastInt = Integer.parseInt(last);

    Integer[] caseSpecial = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19};

    boolean isCaseSpecial = Util.inArray(caseSpecial, real);

    if (isCaseSpecial) {
      result = new String[]{" триллионов ", " миллиардов ", " миллионов ", " тысяч "};
    } else {
      result = Phrase.getNames(lastInt);
    }
    return result;
  }

  /**
   * Получить склонение чисел
   *
   * @param lastNumber Последняя цифра в числовом разряде
   * @return String[] Массив склонений числовых разрядов
   * @since 1.0
   */
  public static String[] getNames(Integer lastNumber) {
    String[] result = {};

    Integer[] caseOne = {2, 3, 4};
    Integer[] caseTwo = {5, 6, 7, 8, 9, 0};

    boolean isCaseOne = Util.inArray(caseOne, lastNumber);
    boolean isCaseTwo = Util.inArray(caseTwo, lastNumber);

    if (isCaseOne) {
      result = new String[]{" триллиона ", " миллиарда ", " миллиона ", " тысячи "};
    } else if (isCaseTwo) {
      result = new String[]{" триллионов ", " миллиардов ", " миллионов ", " тысяч "};
    } else {
      result = new String[]{" триллион ", " миллиард ", " миллион ", " тысяча "};
    }

    return result;
  }
}
