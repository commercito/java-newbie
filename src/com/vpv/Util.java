package com.vpv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Util {

  public static String getDigitNumber(ArrayList<String> chunks) {
    StringBuilder result = new StringBuilder();

    for (String chunk : chunks) {
      result.append(" ").append(chunk);
    }

    return result.toString().trim();
  }

  public static String reverseString(String str) {
    return new StringBuilder(str).reverse().toString();
  }

  public static ArrayList<String> getReversedChunks(String number) {
    var chunks = Util.getChunks(number);
    Collections.reverse(chunks);
    return chunks;
  }

  public static ArrayList<String> getChunks(String number) {
    ArrayList<String> arrList = new ArrayList<>();
    String clean = number.replaceAll("\s+", "");
    String numb = Util.reverseString(clean);

    char[] charArray = numb.toCharArray();
    int chunk = 3;

    for (int i = 0; i < charArray.length; i += chunk) {
      char[] digit = Arrays.copyOfRange(charArray, i, Math.min(charArray.length, i + chunk));
      String myStr = new String(digit);
      String revers = Util.reverseString(myStr);
      arrList.add(revers);
    }

    return arrList;
  }
}
